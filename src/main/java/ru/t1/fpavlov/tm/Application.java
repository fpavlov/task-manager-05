package ru.t1.fpavlov.tm;

import static ru.t1.fpavlov.tm.constant.TerminalConst.CMD_ABOUT;
import static ru.t1.fpavlov.tm.constant.TerminalConst.CMD_HELP;
import static ru.t1.fpavlov.tm.constant.TerminalConst.CMD_VERSION;

public class Application {

    public static void main(String[] args) {
        displayWelcomeText();
        run(args);
    }

    private static void displayWelcomeText() {
        System.out.println("**WELCOME TO TASK MANAGER**");
    }

    private static void displayHelp() {
        System.out.format(
                "Commands: %n" +
                        "\t %s - Display program version;%n" +
                        "\t %s - Display developer info%n" +
                        "\t %s - Display list of terminal commands%n",
                CMD_VERSION,
                CMD_ABOUT,
                CMD_HELP
        );

        System.exit(0);
    }

    private static void displayAbout() {
        System.out.println("Pavlov Philipp");
        System.out.println("fpavlov@t1-consulting.ru");
        System.exit(0);
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
        System.exit(0);
    }

    private static void displayErrorMessage() {
        System.err.println("Unexpected parameter or parameter is missing");
        System.exit(0);
    }

    private static void run(final String[] args) {
        if (args == null || args.length == 0) {
            displayErrorMessage();
            return;
        }

        final String param = args[0];

        switch (param) {
            case CMD_HELP:
                displayHelp();
                break;
            case CMD_ABOUT:
                displayAbout();
                break;
            case CMD_VERSION:
                displayVersion();
                break;
            default:
                displayErrorMessage();
        }
    }

}
